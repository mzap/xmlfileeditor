package pl.mzap.model;

import lombok.*;
import pl.mzap.adapter.LocalDateAdapter;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.Objects;

@NoArgsConstructor
@ToString
@AllArgsConstructor
public class Student {

    @Setter
    @Getter
    private String firstName;
    @Setter
    @Getter
    private String lastName;
    @Setter
    private LocalDate birthDate;

    @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
    public LocalDate getBirthDate() {
        return birthDate;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if (getClass() != obj.getClass())
            return false;
        Student student = (Student)obj;
        return Objects.equals(firstName.toLowerCase(), student.firstName.toLowerCase())
                && Objects.equals(lastName.toLowerCase(), student.lastName.toLowerCase());
    }
}
