package pl.mzap.model;

import lombok.*;
import pl.mzap.adapter.LocalDateAdapter;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;

@NoArgsConstructor
@ToString
@AllArgsConstructor
public class Book {

    @Setter @Getter private String title;
    @Setter @Getter private String genre;
    @Setter private LocalDate releaseDate;
    @Setter @Getter private boolean available;

    @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void rentBook(Student student){
        this.available = false;
//        this.owner = student;
    }

    public void returnBook() {
        this.available = true;
//        this.owner = null;
    }
}
