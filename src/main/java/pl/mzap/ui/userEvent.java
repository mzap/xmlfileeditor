package pl.mzap.ui;

import pl.mzap.database.Database;
import pl.mzap.model.Book;
import pl.mzap.model.Student;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class userEvent {

    private Scanner scanner = new Scanner(System.in);
    private Database database;

    public String getChooseMenuItem() {
        System.out.print("Choose menu item: ");
        return scanner.nextLine();
    }

    public void addStudent() {
        String firstName, lastName;
        LocalDate birthDate;

        System.out.print("First name: ");
        firstName = scanner.nextLine();
        System.out.print("Last name: ");
        lastName = scanner.nextLine();
        System.out.print("Birth date: (yyyy-dd-mm)");
        birthDate = LocalDate.parse(scanner.nextLine());

        Student student = new Student(firstName, lastName, birthDate);
        database.addStudent(student);
    }

    public void listBooks() {
        database.getBooks().stream()
                .filter(Book::isAvailable)
                .collect(Collectors.toList())
                .forEach(System.out::println);
    }

    public void listSortedBooks() {
        List<Book> sortedBooks = new ArrayList<>(database.getBooks());
        sortedBooks.sort(Comparator.comparing(Book::getTitle));
        sortedBooks.forEach(System.out::println);
    }

    public void rentBook() {
        Student student = findStudent();
        if(student != null){
            Book book = findBookByTitle();
            if(book != null){
                book.rentBook(student);
            }
        }
    }

    public void returnBook() {
        Book book = findBookByTitle();
        if (book != null)
            book.returnBook();
    }

    private Student findStudent() {
        String firstName, lastName;
        LocalDate birthDate;

        System.out.print("First name: ");
        firstName = scanner.nextLine();
        System.out.print("Last name: ");
        lastName = scanner.nextLine();
        System.out.print("Birth date: (yyyy-dd-mm)");
        birthDate = LocalDate.parse(scanner.nextLine());
        Student student = new Student(firstName, lastName, birthDate);

        return database.getStudents().stream().filter(s -> s.equals(student)).findFirst().orElse(null);
    }

    private Book findBookByTitle() {
        System.out.print("Title of book: ");
        String title = scanner.nextLine();

        return database.getBooks().stream().filter(b -> b.getTitle().toLowerCase().equals(title.toLowerCase())).findFirst().orElse(null);
    }

    public void setDatabase(Database database) {
        this.database = database;
    }

}
