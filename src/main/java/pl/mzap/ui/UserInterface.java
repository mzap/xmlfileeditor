package pl.mzap.ui;

public class UserInterface {

    public static final String ADD_STUDENT = "1";
    public static final String LIST_BOOKS = "2";
    public static final String LIST_SORTED_BOOKS = "3";
    public static final String RENT_BOOK = "4";
    public static final String RETURN_BOOK = "5";
    public static final String SAVE_DATA = "6";
    public static final String CLOSE_APP = "0";

    public static void printExitMessage() {
        System.out.println("Program closed");
    }

    public void printMenu() {
        System.out.println("----------------");
        System.out.println(ADD_STUDENT + " ADD STUDENT");
        System.out.println(LIST_BOOKS + " LIST BOOKS");
        System.out.println(LIST_SORTED_BOOKS + " SORT BOOKS");
        System.out.println(RENT_BOOK + " RENT BOOK");
        System.out.println(RETURN_BOOK + " RETURN BOOK");
        System.out.println(SAVE_DATA + " SAVE DATA");
        System.out.println(CLOSE_APP + " CLOSE APP");
    }

    public void printSaveMessage() {
        System.out.println("Data saved!");
    }
}
