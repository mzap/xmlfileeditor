package pl.mzap.wrapper;

import lombok.NoArgsConstructor;
import pl.mzap.model.Book;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "books")
@NoArgsConstructor
public class BookWrapper {

    private List<Book> books = new ArrayList<>();

    @XmlElements(@XmlElement(name = "book"))
    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
