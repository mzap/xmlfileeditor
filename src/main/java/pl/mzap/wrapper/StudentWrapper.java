package pl.mzap.wrapper;

import lombok.NoArgsConstructor;
import pl.mzap.model.Student;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "students")
@NoArgsConstructor
public class StudentWrapper {

    private List<Student> students = new ArrayList<>();

    @XmlElements(@XmlElement(name = "student"))
    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

}
