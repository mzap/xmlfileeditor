package pl.mzap;

import pl.mzap.database.Database;
import pl.mzap.database.XMLAccess;
import pl.mzap.model.Book;
import pl.mzap.model.Student;
import pl.mzap.ui.userEvent;
import pl.mzap.ui.UserInterface;

import javax.xml.bind.JAXBException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class Library {

    private static XMLAccess xmlAccess = new XMLAccess();
    private static Database database = new Database();
    private static UserInterface userInterface = new UserInterface();
    private static userEvent userEvent = new userEvent();

    public static void main(String[] args) throws JAXBException {

//        initListDefaultData();
        database = xmlAccess.getDataFomFile();
        userEvent.setDatabase(database);
        String chooseItem;
        do {

            userInterface.printMenu();
            chooseItem = userEvent.getChooseMenuItem();

            switch (chooseItem) {
                case UserInterface.ADD_STUDENT:
                    userEvent.addStudent();
                    break;
                case UserInterface.LIST_BOOKS:
                    userEvent.listBooks();
                    break;
                case UserInterface.LIST_SORTED_BOOKS:
                    userEvent.listSortedBooks();
                    break;
                case UserInterface.RENT_BOOK:
                    userEvent.rentBook();
                    break;
                case UserInterface.RETURN_BOOK:
                    userEvent.returnBook();
                    break;
                case UserInterface.CLOSE_APP:
                    UserInterface.printExitMessage();
                    break;
                case UserInterface.SAVE_DATA:
                    userInterface.printSaveMessage();
                    xmlAccess.saveDataToFile(database);
                    break;
                default:
                    UserInterface.printExitMessage();
                    break;
            }

        } while (!chooseItem.equals("0"));

    }

    private static void initListDefaultData() {
        List<Student> initStudent = Arrays.asList(
                new Student("Jan", "Kowalski", LocalDate.of(1990, 1, 2)),
                new Student("Michal", "Nowak", LocalDate.of(1990, 1, 2))
        );

        List<Book> initBooks = Arrays.asList(
                new Book("Clean code", "document", LocalDate.of(2004, 1, 1), true),
                new Book("Java Basic", "lecture", LocalDate.of(2001, 1, 1), true)
        );
        database.setStudents(initStudent);
        database.setBooks(initBooks);
    }

}
