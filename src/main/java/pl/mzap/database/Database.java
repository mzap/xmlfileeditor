package pl.mzap.database;

import pl.mzap.model.Book;
import pl.mzap.model.Student;
import pl.mzap.wrapper.BookWrapper;
import pl.mzap.wrapper.StudentWrapper;

import java.util.List;
import java.util.stream.Collectors;

public class Database {

    private StudentWrapper studentWrapper = new StudentWrapper();
    private BookWrapper bookWrapper = new BookWrapper();

    public StudentWrapper getStudentWrapper() {
        return studentWrapper;
    }

    public void setStudentWrapper(StudentWrapper studentWrapper) {
        this.studentWrapper = studentWrapper;
    }

    public BookWrapper getBookWrapper() {
        return bookWrapper;
    }

    public void setBookWrapper(BookWrapper books) {
        this.bookWrapper = books;
    }

    public List<Student> getStudents() {
        return studentWrapper.getStudents();
    }

    public void setStudents(List<Student> students) {
        this.studentWrapper.setStudents(students);
    }

    public List<Book> getBooks() {
        return bookWrapper.getBooks();
    }

    public void setBooks(List<Book> books) {
        this.bookWrapper.setBooks(books);
    }

    public void addStudent(Student student) {
        List<Student> existedStudents = studentWrapper.getStudents().stream()
                .filter(s -> s.equals(student))
                .collect(Collectors.toList());
        if (existedStudents.size() > 0)
            System.out.println("Student existing");
        else
            studentWrapper.getStudents().add(student);
    }

}
