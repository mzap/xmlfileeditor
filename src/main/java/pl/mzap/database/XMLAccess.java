package pl.mzap.database;

import pl.mzap.wrapper.BookWrapper;
import pl.mzap.wrapper.StudentWrapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class XMLAccess {

    private JAXBContext jaxbContext;
    private Marshaller marshaller;
    private Unmarshaller unmarshaller;

    public void saveDataToFile(Database database) throws JAXBException {
        saveStudents(database.getStudentWrapper());
        saveBooks(database.getBookWrapper());
    }

    public Database getDataFomFile() throws JAXBException {
        Database database = new Database();
        database.setStudentWrapper(getStudentsFromFile());
        database.setBookWrapper(getBooksFromFile());
        return database;
    }

    private void saveStudents(StudentWrapper students) throws JAXBException {
        this.jaxbContext = JAXBContext.newInstance(StudentWrapper.class);
        this.marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(students, System.out);
        marshaller.marshal(students, new File("./students.xml"));
    }

    private void saveBooks(BookWrapper books) throws JAXBException {
        this.jaxbContext = JAXBContext.newInstance(BookWrapper.class);
        this.marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(books, System.out);
        marshaller.marshal(books, new File("./books.xml"));
    }

    private StudentWrapper getStudentsFromFile() throws JAXBException {
        this.jaxbContext = JAXBContext.newInstance(StudentWrapper.class);
        this.unmarshaller = jaxbContext.createUnmarshaller();
        return (StudentWrapper) unmarshaller.unmarshal(new File("./students.xml"));
    }

    private BookWrapper getBooksFromFile() throws JAXBException {
        this.jaxbContext = JAXBContext.newInstance(BookWrapper.class);
        this.unmarshaller = jaxbContext.createUnmarshaller();
        return (BookWrapper) unmarshaller.unmarshal(new File("./books.xml"));
    }

}
